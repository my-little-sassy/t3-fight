import Dependencies._

ThisBuild / scalaVersion     := "2.13.11"
ThisBuild / version          := "0.1.0-SNAPSHOT"
ThisBuild / organization     := "my-little-sassy"
ThisBuild / organizationName := "My Little Sassy"

lazy val `t3-fight` = (project in file("."))
  .settings(
    name := "t3 fight",
    libraryDependencies ++= cats
      ++ fs2
      ++ httpServer
      ++ json
      ++ lens
      ++ utils
  )
