package t3

import cats.effect.kernel.{Ref, Resource}
import cats.effect.{ExitCode, IO, IOApp}
import com.softwaremill.macwire._
import t3.api.{ApiImpl, Authenticator, IdGenerator}
import t3.api.admin.{AdminApi, AdminService}
import t3.api.bot.{BotApi, BotService}

case class AppConfig(host: String, port: Int) extends ServerConfig

object Main extends IOApp {

  def readConfig: Resource[IO, AppConfig] = Resource.eval(IO {
    AppConfig("localhost", 8080)
  })

  lazy val resources: Resource[IO, Unit] = for {
    config <- readConfig
    state  <- Resource.eval(Ref.of[IO, State](State.empty))
    apis   <- wireApis(state)
    server <- ServerBuilder(config, apis)
  } yield {}

  override def run(args: List[String]): IO[ExitCode] = {
    resources.useForever.as(ExitCode.Success)
  }

  def wireApis(state: Ref[IO, State]): Resource[IO, List[ApiImpl]] = Resource.eval(IO {

    lazy val authenticator: Authenticator = wire[Authenticator]

    lazy val idGenerator: IdGenerator = wire[IdGenerator]

    lazy val adminService: AdminService = wire[AdminService]
    lazy val adminAPi: AdminApi         = wire[AdminApi]

    lazy val botService: BotService = wire[BotService]
    lazy val botApi: BotApi         = wire[BotApi]

    wireSet[ApiImpl].toList
  })

}
