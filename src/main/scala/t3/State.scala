package t3

import t3.Game.{Board, CellType}

import scala.io.StdIn

case class State(lobby: Lobby, games: List[Game])

object State {
  def empty: State = State(Lobby(List.empty), List.empty)
}

case class Lobby(players: List[Player])

case class Player(id: String, displayName: String, token: String)

sealed trait GameError

object GameError {
  case object InvalidMove extends GameError
}

case class Players(x: Player, o: Player) {
  def contains(p: Player): Boolean = Set(x, o).contains(p)
}

sealed trait Game {
  val id: String
  val moves: List[Move]
  val players: Players
  val board: Board

  def contains(player: Player): Boolean = players.contains(player)

  def playMove(move: Move): Either[GameError, Game]

  val currentPlayer: Player
}

object Game {

  sealed trait CellType
  object CellType {
    case object Empty extends CellType
    case object X extends CellType
    case object O extends CellType
  }

  type Board = List[List[CellType]]

  implicit class BoardOps(b: Board) {
    def apply(x: Int, y: Int): CellType = b(x)(y)
  }

  def takeCell(cell: Cell, player: CellType, board: Board): Board = {
    val Cell(x, y) = cell
    board.updated(x, board(x).updated(y, player))
  }

  def checkCell(cell: Cell, board: Board): Either[GameError, Cell] = {
    val Cell(x, y) = cell
    if ((0 to 2).contains(x) && (0 to 2).contains(y) && board(x)(y) == CellType.Empty) Right(cell)
    else Left(GameError.InvalidMove)
  }

  def checkGameOver(board: Board): Option[Outcome] = {
    def checkLine(cells: List[CellType]): Boolean = cells.forall(c => c == cells.head && c != CellType.Empty)

    def checkRows(board: Board) = board.find(checkLine)

    def checkCols(board: Board) = board.transpose.find(checkLine)

    def checkDiags(board: Board) = {
      List(
        List(board(0, 0), board(1, 1), board(2, 2)),
        List(board(2, 0), board(1, 1), board(0, 2)),
      ).find(checkLine)
    }

    def checkDraw(board: Board): Option[Outcome] =
      if (board.forall(_.forall(_ != CellType.Empty))) Some(Outcome.Draw)
      else None

    checkRows(board)
      .orElse(checkCols(board))
      .orElse(checkDiags(board))
      .map(_.head)
      .map(Outcome.Winner)
      .orElse(checkDraw(board))
  }

  object Board {
    def empty: List[List[CellType]] = List.fill(3)(List.fill(3)(CellType.Empty))
  }

  case class Started(id: String, players: Players) extends Game {
    val moves: List[Move] = List.empty
    lazy val currentPlayer: Player = players.x
    lazy val board: Board = Board.empty

    override def playMove(move: Move): Either[GameError, Game] = {
      checkCell(move.cell, board)
        .map(takeCell(_, CellType.X, board))
        .map(OnGoing(id, players, List(move), _))
    }
  }

  case class OnGoing(id: String, players: Players, moves: List[Move], board: Board) extends Game {
    lazy val currentTurn: Player =
      if (moves.last.player == players.x) players.o
      else players.x

    override lazy val currentPlayer: Player =
      if (moves.last.player == players.x) players.o else players.x

    override def playMove(move: Move): Either[GameError, Game] = {
      val player = if (move.player == players.x) CellType.X else CellType.O
      checkCell(move.cell, board)
        .map(takeCell(_, player, board))
        .map { board =>
          checkGameOver(board) match {
            case Some(outcome) =>
              Finished(id, players, moves.appended(move), board, outcome)
            case None =>
              OnGoing(id, players, moves.appended(move), board)
          }
        }
    }
  }

  case class Finished(id: String, players: Players, moves: List[Move], board: Board, outcome: Outcome) extends Game {
    override lazy val currentPlayer: Player =
      if (moves.last.player == players.x) players.o else players.x

    override def playMove(move: Move): Either[GameError, Game] = Left(GameError.InvalidMove)
  }

  sealed trait Outcome
  object Outcome {
    case object Draw extends Outcome
    case class Winner(player: CellType) extends Outcome
  }

}

case class Move(player: Player, cell: Cell)

case class Cell(x: Int, y: Int)

object TestGame {
  def main(args: Array[String]): Unit = {
    val px = Player("x", "X", "xx")
    val po = Player("o", "O", "oo")


    play(Game.Started("abc", Players(px, po))) match {
      case Left(value) =>
        println(s"Oh, no! $value")
        play(Game.Started("abc", Players(px, po)))
      case Right(_)    => println("Game over, punk!")
    }
  }

  def play(game: Game): Either[GameError, Game] = {
    println(s"It's ${game.currentPlayer}'s turn'")
    val Array(x, y) = StdIn.readLine("Write your move, punk! ").split(",").map(_.trim.toInt)
    game.playMove(Move(game.currentPlayer, Cell(x, y)))
      .map { g =>
        println(show(g))
        g
      }
      .flatMap {
        case f: Game.Finished =>
          println(s"Game finished: $f")
          Right(f)
        case next =>
          play(next)
      }
  }

  def show(game: Game): String = {
    game.board.map(_.map(show).mkString("| ", " | ", " |")).mkString("\n" ++ "-" * 13 ++ "\n")
  }

  def show(cell: CellType): String = cell match {
    case CellType.Empty => " "
    case CellType.X     => "X"
    case CellType.O     => "O"
  }
}
