package t3.api.bot

import java.util.UUID

import cats.data.EitherT
import cats.effect.{IO, Ref}
import com.typesafe.scalalogging.LazyLogging
import monocle.Focus
import t3._
import t3.api.{ApiError, BadRequestError, IdGenerator, NotFoundError}

class BotService(state: Ref[IO, State], idGen: IdGenerator) extends LazyLogging {

  private val playersFocus = Focus[State](_.lobby.players)
  private val gamesFocus   = Focus[State](_.games)

  def login(request: LoginRequest): EitherT[IO, ApiError, LoginResponse] = {
    def playerExists(s: State): EitherT[IO, ApiError, State] = {
      EitherT.cond[IO](
        test  = s.lobby.players.forall(_.displayName != request.displayName),
        right = s,
        left  = BadRequestError(s"Display name ${request.displayName} already in use")
      )
    }

    def insertPlayer(name: String): EitherT[IO, ApiError, String] = {
      def makePlayer(name: String): Player = Player(idGen.next(), name, UUID.randomUUID().toString)

      for {
        player <- EitherT.rightT[IO, ApiError](makePlayer(name))
        _      <- EitherT.right(state.update(playersFocus.modify(_.appended(player))))
      } yield player.token
    }

    EitherT.right[ApiError](state.get)
      .flatMap(playerExists)
      .flatMap(_ => insertPlayer(request.displayName))
      .map(token => LoginResponse(token))
  }

  def matchStatus(player: Player)(u: Unit): EitherT[IO, ApiError, MatchStatus] = EitherT.right {
    state.get.map { s =>
      MatchStatus(
        gameId = s.games.find(_.contains(player)).map(_.id),
        players = s.lobby.players.size
      )
    }
  }

  def gameStatus(player: Player)(gameId: String): IO[Either[ApiError, GameStatus]] = {
    state.get.map(_.games.find(g => g.id == gameId && g.contains(player)))
      .map(_.toRight(NotFoundError()))
      .map(_.map(g => GameStatus.from(g, player)))
  }

  def move(player: Player)(move: MakeMove): EitherT[IO, ApiError, Unit] = {
    def validateGame(move: MakeMove): EitherT[IO, ApiError, Game] = EitherT {
      state.get.map(_.games.find(_.id == move.gameId))
        .map(_.toRight[ApiError](NotFoundError()))
    }

    def validateTurn(player: Player, game: Game): EitherT[IO, ApiError, Move] = {
      EitherT.cond[IO](
        test  = game.currentPlayer == player,
        right = Move(player, move.cell.model),
        left  = BadRequestError("Not your turn")
      )
    }

    def updateGame(game: Game, move: Move): EitherT[IO, ApiError, Game] = {
      EitherT.fromEither[IO](game.playMove(move))
        .leftMap[ApiError] {
          case GameError.InvalidMove => BadRequestError("Invalid move")
        }
    }

    def storeGame(game: Game): EitherT[IO, ApiError, Unit] = EitherT.rightT {
      state.update(gamesFocus.modify { games =>
        games.filter(_.id != game.id).appended(game)
      })
    }

    def logStuff(prev: Game, next: Game): EitherT[IO, ApiError, Unit] = EitherT.rightT {
      logger.info(
        s"""
           |Previous game: $prev
           |Current game:  $next
           |""".stripMargin)
    }

    for {
      game <- validateGame(move)
      m    <- validateTurn(player, game)
      next <- updateGame(game, m)
      _    <- storeGame(next)
      _    <- logStuff(game, next)
    } yield {}
  }

}
