package t3.api.bot

import cats.effect.IO
import sttp.model.StatusCode
import sttp.tapir.Endpoint
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.server.ServerEndpoint
import t3.api.{ApiDefinition, ApiError, ApiImpl, Authenticator, DocumentedApi}

object BotApi extends ApiDefinition {

  private lazy val authenticated = baseEndpoint.securityIn(header[String]("X-BOT-TOKEN"))

  lazy val login: Endpoint[Unit, LoginRequest, ApiError, LoginResponse, Any] =
    baseEndpoint.in("login").post
      .in(jsonBody[LoginRequest])
      .out(jsonBody[LoginResponse])

  lazy val matchStatus: Endpoint[String, Unit, ApiError, MatchStatus, Any] =
    authenticated.get
      .in("match")
      .out(jsonBody[MatchStatus])

  lazy val gameStatus: Endpoint[String, String, ApiError, GameStatus, Any] =
    authenticated.get
      .in("game" / path[String]("gameId"))
      .out(jsonBody[GameStatus])

  lazy val move: Endpoint[String, MakeMove, ApiError, Unit, Any] =
    authenticated.post
      .in("game" / path[String]("gameId"))
      .in(jsonBody[Cell])
      .mapInTo[MakeMove]
      .out(statusCode(StatusCode.Accepted))

}

class BotApi(service: BotService, auth: Authenticator) extends ApiImpl with DocumentedApi {
  override val endpoints: List[ServerEndpoint[Any, IO]] = List(
    BotApi.login.serverLogic(service.login(_).value),
    BotApi.matchStatus
      .serverSecurityLogic(auth.authenticate)
      .serverLogic(p => service.matchStatus(p)(_).value),
    BotApi.gameStatus
      .serverSecurityLogic(auth.authenticate)
      .serverLogic(service.gameStatus),
    BotApi.move
      .serverSecurityLogic(auth.authenticate)
      .serverLogic(p => service.move(p)(_).value),
  )
}
