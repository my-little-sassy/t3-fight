package t3.api

import t3.Game.Outcome
import t3.{Game, Player, Players}

package object bot {

  // Login
  case class LoginRequest(displayName: String)

  case class LoginResponse(token: String)

  // Match status
  case class MatchStatus(gameId: Option[String], players: Int)

  // Game status
  sealed trait GameStatus

  case class Started(
    gameId: String,
    opponent: String,
    playersTurn: Boolean
  ) extends GameStatus

  case class OnGoing(
    gameId: String,
    playersTurn: Boolean,
    lastMove: Cell
  ) extends GameStatus

  case class Finished(
    gameId: String,
//    outcome: Outcome
  ) extends GameStatus

  case class Cell(x: Int, y: Int) {
    def model: t3.Cell = t3.Cell(x, y)
  }

  object Cell {
    def from(c: t3.Cell): Cell = Cell(c.x, c.y)
  }

  object GameStatus {
    def from(game: Game, player: Player): GameStatus = game match {
      case Game.Started(id, Players(x, o)) =>
        val opponent = if (player == x) o else x
        Started(id, opponent.displayName, player == x)

      case Game.OnGoing(id, _, moves, _) =>
        val lastMove = moves.last
        OnGoing(id, lastMove.player != player, Cell.from(lastMove.cell))

      case Game.Finished(id, _, _, _, _) =>
        Finished(id)
    }
  }

  // Move
  case class MakeMove(gameId: String, cell: Cell)
}
