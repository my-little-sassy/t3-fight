package t3

import cats.effect.IO
import io.circe.generic.AutoDerivation
import org.http4s.HttpRoutes
import sttp.model.StatusCode
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.json.circe.TapirJsonCirce
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.server.http4s.Http4sServerInterpreter
import sttp.tapir.{Endpoint, Tapir}

package object api {

  // Errors

  sealed trait ApiError

  case class BadRequestError(message: String) extends ApiError

  case class UnauthorizedError() extends ApiError

  case class NotFoundError(message: String = "Not Found") extends ApiError


  // API definitions

  trait ApiDefinition extends Tapir with TapirJsonCirce with AutoDerivation {

    protected lazy val baseEndpoint: Endpoint[Unit, Unit, ApiError, Unit, Any] =
      endpoint.errorOut(
        oneOf[ApiError](
          oneOfVariant(StatusCode.BadRequest, jsonBody[BadRequestError]),
          oneOfVariant(StatusCode.Unauthorized, jsonBody[UnauthorizedError]),
          oneOfVariant(StatusCode.NotFound, jsonBody[NotFoundError])
        )
      )

  }

  trait ApiImpl {

    val endpoints: List[ServerEndpoint[Any, IO]]

    def apply(interpreter: Http4sServerInterpreter[IO]): HttpRoutes[IO] = {
      interpreter.toRoutes(endpoints)
    }

  }

  trait DocumentedApi {

    val endpoints: List[ServerEndpoint[Any, IO]]

  }

}
