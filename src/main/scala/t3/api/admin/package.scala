package t3.api

package object admin {

  case class StartGameRequest(playerX: String, playerO: String)

  case class StartGameResponse(gameId: String)

}
