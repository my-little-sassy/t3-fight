package t3.api.admin

import cats.data.EitherT
import cats.effect.{IO, Ref}
import cats.implicits._
import monocle.Focus
import t3.api.{ApiError, IdGenerator, NotFoundError}
import t3.{Game, Players, State}

class AdminService(state: Ref[IO, State], idGen: IdGenerator) {

  def getState(): EitherT[IO, ApiError, State] = {
    EitherT.right(state.get)
  }

  private val GamesFocus = Focus[State](_.games)

  def startGame(request: StartGameRequest): EitherT[IO, ApiError, StartGameResponse] = {
    EitherT.right(state.get)
      .flatMap { state =>
        val players = for {
          x <- state.lobby.players.find(_.id == request.playerX)
          o <- state.lobby.players.find(_.id == request.playerO)
        } yield Players(x, o)

        EitherT.fromOption[IO](players, NotFoundError()).leftWiden[ApiError]
      }
      .map(Game.Started(idGen.next(), _))
      .semiflatTap(game => state.update { s =>
        GamesFocus.modify(_.appended(game))(s)
      })
      .map(g => StartGameResponse(g.id))
  }

}
