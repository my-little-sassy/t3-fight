package t3.api.admin

import cats.effect.IO
import sttp.tapir.Endpoint
import sttp.tapir.generic.auto.schemaForCaseClass
import sttp.tapir.server.ServerEndpoint
import t3.State
import t3.api.{ApiDefinition, ApiError, ApiImpl, DocumentedApi}

object AdminApi extends ApiDefinition {

  lazy val getState: Endpoint[Unit, Unit, ApiError, State, Any] =
    baseEndpoint.get
      .in("admin" / "state")
      .out(jsonBody[State])

  lazy val startGame: Endpoint[Unit, StartGameRequest, ApiError, StartGameResponse, Any] =
    baseEndpoint.post
      .in("admin" / "start")
      .in(jsonBody[StartGameRequest])
      .out(jsonBody[StartGameResponse])

}

class AdminApi(service: AdminService) extends ApiImpl with DocumentedApi {

  override val endpoints: List[ServerEndpoint[Any, IO]] = List(
    AdminApi.getState.serverLogic(_ => service.getState().value),
    AdminApi.startGame.serverLogic(service.startGame(_).value),
  )

}
