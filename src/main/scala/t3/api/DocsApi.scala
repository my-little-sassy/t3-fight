package t3.api

import cats.effect.IO
import org.http4s.HttpRoutes
import sttp.tapir.server.ServerEndpoint
import sttp.tapir.server.http4s.Http4sServerInterpreter
import sttp.tapir.swagger.SwaggerUIOptions
import sttp.tapir.swagger.bundle.SwaggerInterpreter

object DocsApi {

  private val options = SwaggerUIOptions.default.copy(
    contextPath = List("docs"),
    pathPrefix = List("service")
  )

  def apply(apis: List[ServerEndpoint[Any, IO]]): HttpRoutes[IO] = {
    val impl = new ApiImpl {
      override val endpoints: List[ServerEndpoint[Any, IO]] =
        SwaggerInterpreter(swaggerUIOptions = options)
          .fromServerEndpoints[IO](apis, "API", "1.0")
    }
    impl.apply(Http4sServerInterpreter[IO]())
  }

}
