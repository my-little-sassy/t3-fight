package t3.api

import scala.util.Random

class IdGenerator {

  def next(): String = Random.alphanumeric.take(5).mkString

}
