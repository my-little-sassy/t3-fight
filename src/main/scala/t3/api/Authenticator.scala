package t3.api

import cats.data.EitherT
import cats.effect.{IO, Ref}
import t3.{Player, State}

class Authenticator(state: Ref[IO, State]) {

  def authenticate(token: String): IO[Either[ApiError, Player]] = {
    state.get
      .map(_.lobby.players.find(_.token == token))
      .map(_.toRight(UnauthorizedError()))
  }

}
